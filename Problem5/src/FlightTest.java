
//********************************************************************
//File:         FlightTest.java       
//Author:       Liam Quinn
//Date:         October 5th
//Course:       CPS100
//
//Problem Statement:
// Design and implement a class called Flight that represents an airline flight. 
// It should contain instance data that represents the airline name, flight 
// number, and the flight's origin and destination cities. Define the Flight 
// constructor to accept and initialize all instance data. Include getter and 
// setter methods for all instance data. Include a toString method that returns 
// a one-line description of the flight. Create a driver class called FlightTest, 
// whose main method instantiates and updates several Flight objects.
//
//Inputs: None  
//Outputs: Results of testing Flight's methods
// 
//********************************************************************

public class FlightTest
{

  public static void main(String[] args)
  {
    Flight comoxCalgary = new Flight("WestJet", 123, "Comox", "Calgary");
    Flight comoxVictoria = new Flight("Air Canada", 456, "Comox", "Victoria");
    
    System.out.println("Flight " + comoxCalgary);
    System.out.println("Flight " + comoxVictoria);
        
    //test setter
    comoxCalgary.setAirlineName("Frank's Fleet");
    System.out.println("Airline name: " + comoxCalgary.getAirlineName());
    
    //test setter with an empty string
    comoxCalgary.setAirlineName("");
    System.out.println("Airline name: " + comoxCalgary.getAirlineName());
  }

}


public class Flight
{
  private String airlineName;
  private int flightNumber;
  private String originCity;
  private String destinationCity;
  
  // Constructor
  public Flight(String airline, int flightNum, String strFrom, String strTo)
  {
    airlineName = airline;
    flightNumber = flightNum;
    originCity = strFrom;
    destinationCity = strTo;
  }
  
  public void setAirlineName(String strName)
  {
    if (!strName.isEmpty())
      airlineName = strName;
  }
  
  public String getAirlineName()
  {
    return airlineName;
  }
  
  
  // toString
  public String toString()
  {
    String strTmp = "";
    strTmp += "Airline: " + airlineName;
    strTmp += "  Flight Number: " + flightNumber;    
    strTmp += "  Origin: " + originCity; 
    strTmp += "  Destination: " + destinationCity; 
    
    return strTmp;
  }
}

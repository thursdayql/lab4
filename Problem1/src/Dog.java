
public class Dog
{
  // Dog's instance variable - should be ! private !
  private String name;
  private int age;
  private final int DOG_HUMAN_AGE_FACTOR = 7;
  
  // Create parameterless constructor - should be public
  public Dog()
  {
    name = "NoNameDog";
    age = -1;
  }
  
  // Create Dog's constructor with correct parameters
  public Dog(String strName, int iAge)
  {
    name = strName;
    age = iAge;
  }
  
  // Create a setter for the name instance variable
  public void setName(String strName)
  {
    name = strName;
  }
  
  // Create a setter for the age instance variable
  public void setAge(int iNewAge)
  {
    age = iNewAge;
  }
  
  // Create a getter for Dog's name
  public String getName()
  {
    return name;
  }
  
  // Create a getter for Dog's age
  public int getAge()
  {
    return age;
  }
  
  // A method to return dog's age in human aage (i.e. times 7)
  public int getHumanAgeOfDog()
  {
    return age * DOG_HUMAN_AGE_FACTOR;
  }
  
  
  // Create toString method - should be public
  public String toString()
  {
    String str = "";
    str = str + "Name: " + name;
    str = str + "  Age= " + age;
    
    return str;
  }
}


//********************************************************************
//File:         Kennel.java       
//Author:       Liam Quinn
//Date:         September 28th
//Course:       CPS100
//
//Problem Statement:
//Design and implement a class called Dog that contains instance data that 
//represents the dog's  name and age. Define the Dog constructor to accept 
//and initialize instance data. Include getter and setter methods for the 
//name and age. Include a method to compute and return the age of the dog 
//in person years (seven times the dogs age). Include a toString method 
//that returns a one-line description of the dog. Create a driver class 
//called Kennel, whose main method instantiates and updates several Dog objects.
//
//Inputs: None  
//Outputs: Results of testing all dog's methods
// 
//********************************************************************



public class Kennel
{

  public static void main(String[] args)
  {
    Dog alex = new Dog();
    Dog alice = new Dog("Sadie", 2);
    
    System.out.println("Dog Alex is " + alex);
    System.out.println("Dog Alice is " + alice.toString());
    
    alex.setName("Spring");
    System.out.println("Dog Alex is " + alex);
    
    alex.setAge(6);
    System.out.println("Dog Alex is " + alex);
    
    String dogName = alice.getName();
    System.out.println("Alice's name is " + dogName);
    
    int dogAge = alex.getAge();
    System.out.println("Alex's age is " + dogAge);
    
    int iHumanAge = alex.getHumanAgeOfDog();
    System.out.println("Alex's human age is " + iHumanAge);
    
  }

}


public class DoubleSecret
{
  private Secret firstSecret;
  private Secret secondSecret;

  public DoubleSecret()
  {
    firstSecret = new Secret();
    secondSecret = new Secret();
  }

  public void changeSecret()
  {
    firstSecret = new Secret();
    secondSecret = new Secret();
  }

  public void changeFirstSecret()
  {
    firstSecret = new Secret();
  }

  public void changeSecondsSecret()
  {
    secondSecret = new Secret();
  }

  // setter and getter for the first secret of this double secret
  public String getFirstSecret()
  {
    String str = "" +  firstSecret.getSecretChar() + firstSecret.getSecretInt();
    
    return str;
  }
  
  public void setFirstSecret(char myChar, int myInt)
  {
    firstSecret.setSecretChar(myChar);
    firstSecret.setSecretInt(myInt);
  }

  public String toString()
  {
    String str = "Double Secret: " + firstSecret.getSecretChar()
                 + secondSecret.getSecretInt();
    str = str + secondSecret.getSecretChar() + secondSecret.getSecretInt();

    return str;
  }

}

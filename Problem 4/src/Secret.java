import java.util.*;


public class Secret
{
  private char secretChar;
  private int secretInt;
  private Random gen = new Random();
  private final int MAXVALUE = 9;
  private final int MAX_ALPHA = 26;

  public Secret()
  {
    randomize();
  }

  public void randomize()
  {
    secretInt = gen.nextInt(MAXVALUE) + 1;
    int asciiCode = (int) 'A' + gen.nextInt(MAX_ALPHA);
    secretChar = (char) asciiCode;
  }

  public void setSecretChar(char myChar)
  {
    if (('A' <= myChar) && (myChar <= 'Z'))
      secretChar = myChar;
  }

  public char getSecretChar()
  {
    return secretChar;
  }

  public void setSecretInt(int myInt)
  {
    if ((1 <= myInt) && (myInt <= MAXVALUE))
      secretInt = myInt;
  }

  public int getSecretInt()
  {
    return secretInt;
  }

  public String toString()
  {
    return "Secret: " + secretChar + secretInt;
  }

}

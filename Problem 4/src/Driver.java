
//********************************************************************
//File:         Driver.java       
//Author:       Liam Quinn
//Date:         October 5th
//Course:       CPS100
//
//Problem Statement:
// Design and implement a class called Secret that represents a single
// upper case character and an integer value between 1 and 9. Define the Secret
// constructor to accept and initialize instance data. Include getters and
// setters for both the character and the integer value. Create a method
// called randomize that randomly changes the instance data. The method
// toString needs to be implemented. Now, create a class called DoubleSecret
// that contains two Secret instance variables. Create the constructor,
// setters, getters, toString, and changeSecret methods. Create methods to
// randomly change each single Secret instance variable. Finally, create
// a Driver class that exercises all methods of both the Secret and
// DoubleSecret objects.
//
//Inputs: None
//Outputs: Results of testing of Secret and DoubleSecret methods 
// 
//********************************************************************

public class Driver
{

  public static void main(String[] args)
  {
    Secret mySecret = new Secret();
    DoubleSecret doubleSecret = new DoubleSecret();
    
    System.out.println("Secret: " + mySecret);
    
    // test setter and getter for a secret character
    mySecret.setSecretChar('P');
    System.out.println("Secret Character = " + mySecret.getSecretChar());
    System.out.println("Secret: " + mySecret);
    
    //test setter and getter for a secret number
    mySecret.setSecretInt(3);
    System.out.println("Secret Number = " + mySecret.getSecretInt());
    System.out.println("Secret: " + mySecret);
    
    // create and test a double secret
    System.out.println(doubleSecret);
    doubleSecret.changeSecret();
    System.out.println(doubleSecret);
    
    doubleSecret.changeFirstSecret();
    System.out.println(doubleSecret);
    
    doubleSecret.changeSecondsSecret();
    System.out.println(doubleSecret);
    
    // test setter and getter for the first secret in the double secret
    System.out.println("First Secret: " + doubleSecret.getFirstSecret());

  }

}
